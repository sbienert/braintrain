package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
"github.com/olivere/elastic"
)

func GetAllFlashCards(w http.ResponseWriter, r *http.Request) {
	ela, err := elastic.NewMultiMatchQuery()
}

func GetFlashCardByName(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func PostFlashCard(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func UpdateFlashCard(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}

func DeleteFlashCardByName (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "not implemented yet !")
}
func main() {

	r := mux.NewRouter()
	r.HandleFunc("/flashcards", GetAllFlashCards).Methods("GET")
	r.HandleFunc("/flashcards", PostFlashCard).Methods("POST")
	r.HandleFunc("/flashcards", UpdateFlashCard).Methods("PUT")
	r.HandleFunc("/flashcards", DeleteFlashCardByName).Methods("DELETE")
	r.HandleFunc("/flashcards/{id}", GetFlashCardByName).Methods("GET")
	if err := http.ListenAndServe(":3000", r); err != nil {
		log.Fatal(err)
	}
	
}
